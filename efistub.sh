#!/bin/sh -e

#require-like function
exit_if_missing(){ # $1 = command to be checked
    if ! type "$1" > /dev/null 2>&1; then
        print_msg "EFISTUB: $1 is required!"; 
        exit 2;
    fi
}

#copy to the efi directory
copy_to_esp(){ # $1 = file to copy (path)
    install -Cv "$1" "$FULL_EFI_PATH"
}

#substitute slashes for back slashes
win32path(){ # $1 = string to convert
    echo "$1" | sed 's:/:\\:g'
} # returns the modified string

#print a debug message
print_msg(){ # $1 = the message
    echo >&2 "$1"
}

#extract all entries for kernels in $EFI_PATH
efi_get_list(){ # no params (it's supposed to be cached in a variable
    efibootmgr -v | grep -iF "/File(`win32path "$EFI_PATH/"`" || echo ""
}

#get the entry for the given kernel (or all entries if none is specified)
efi_get_entry_id(){ # $1 = kernel to look for (file name) / empty
    if [ -n "$1" ]; then
        PATTERN="/File(`win32path "$EFI_PATH/$1"`)"
    else
        PATTERN=""
    fi
    
    echo "$BOOTLIST" | grep -iF "$PATTERN" | cut -d'*' -f1 | cut -d't' -f2
} # returns the id(s) for the given entries

#add entry to efi
efi_add_entry(){ # $1 = label to be showed in the efi menu, $2 = kernel path, $3 = kernel params (initrd, rootfs, etc)
    LABEL="$1";KERNEL="$2";PARAM="$3"
    print_msg "add entry $1"
    efibootmgr -c -L "$LABEL" -l "$KERNEL" -u "$PARAM"
}

#remove entry from efi
efi_remove_entry(){ # $1 = entry id (the ones you get from efi_get_entry_id)
    print_msg "remove $1"
    efibootmgr -b "$1" -B
}

#copy kernel's files and add an entry to efi
install_kernel(){ # $1 = the version of the kernel, $2 = kernel path
    VERSION="$1";KERNEL="$2"
    LABEL="$MACHINE_LABEL ($VERSION)"
    PARAM="root=UUID=$ROOTUUID $KERN_PARAM"
    INITRD="`dirname "$KERNEL"`/$RAMDISK_FNAME$VERSION"
    
    copy_to_esp "$KERNEL"
    if [ -e "$INITRD" ]; then # if there's an initrd copy it and add the right params to the kernel
        copy_to_esp "$INITRD"
        PARAM="$PARAM initrd=$(win32path "$EFI_PATH/`basename "$INITRD"`")"
    fi
    
    efi_add_entry "$LABEL" "$(win32path "$EFI_PATH/`basename "$KERNEL"`")" "$PARAM"
}

#delete kernel's files and its efi entry
remove_kernel(){ # $1 = the version of the kernel, $2 = kernel path
    VERSION="$1";KERNEL="`basename "$2"`"
    rm -f "$FULL_EFI_PATH/$RAMDISK_FNAME$VERSION";
    rm -f "$FULL_EFI_PATH/$KERNEL";
    BNUM="`efi_get_entry_id "$KERNEL"`"
    [ -n "$BNUM" ] && efi_remove_entry $BNUM
}

uninstall_all(){
    rm -f "$FULL_EFI_PATH/"*
    
    ALL_ENTRIES="`efi_get_entry_id`"
    for i in $ALL_ENTRIES; do
        efi_remove_entry "$i"
    done;
}

reset_all(){
    uninstall_all
    
    for k in "/boot/vmlinuz-"*; do
        KERNEL=$(basename "$k")
        VERSION=$(echo "$KERNEL" | cut -d'-' -f2-)
        install_kernel "$VERSION" "$k"
    done;
}

#checking dependencies
exit_if_missing "efibootmgr"
exit_if_missing "install"

#loading configuration
. /etc/efistub.conf


RAMDISK_FNAME='initrd.img-' # this is not standardized between distros
FULL_EFI_PATH="$ESP_PATH$EFI_PATH"
ROOTUUID=$(lsblk -no UUID `df --output=source / | tail -1`)
BOOTLIST="`efi_get_list`"

#check what to do
case $0 in
    *ramdisk-upd)
        print_msg "EFISTUB: Updating `basename "$2"`."
        copy_to_esp "$2"
    ;;
    *kern-inst)
        print_msg "EFISTUB: Installing `basename "$2"`."
        install_kernel "$1" "$2"
    ;;
    *kern-rm)
        print_msg "EFISTUB: Removing kernel `basename "$2"`."
        remove_kernel "$1" "$2"
    ;;
    *) # not called by a hook
        case $1 in
        "--uninstall-all") #remove everything
            print_msg "EFISTUB: Remove all kernels from ESP and EFI entries."
            uninstall_all
        ;;
        "--reset-all") #reset and install everything again
            print_msg "EFISTUB: Reinstall all kernels."
            reset_all
        ;;
        *)
            print_msg "Nothing to do."
        ;;
        esac;
    ;;
esac;

exit 0;
